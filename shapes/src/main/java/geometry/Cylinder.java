/**
 * @author Minh Phu Ngo
 */
package geometry;


public class Cylinder implements Shape3d {
    private double radius;
    private double height;
    /**
     * @param Radius 
     * @param Height
     */
    public Cylinder(double radius,double height){
        this.radius = radius;
        this.height = height;
    }
    /**get volume of the cylinder
     * @return the voulme as value
     */
    public double getVolume(){
        return Math.PI*this.radius*this.radius*this.height ;
    }
    /**get area of the cylinder
     * @return the area as value
     */
    public double getSurfaceArea(){
        return Math.PI*2*this.radius*this.radius+2*Math.PI*this.radius*this.height;
    }
    public double getHeight() {
        return height;
    }
    public double getRadius() {
        return radius;
    }

}
