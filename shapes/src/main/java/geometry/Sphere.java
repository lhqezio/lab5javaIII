package geometry;
/**
 * Contains a Sphere Object
 * @author Hoang Quoc Le
 */

public class Sphere implements Shape3d {
    private double radius;
    /**
     * @param radius
     */
    public Sphere(double radius){
        this.radius=radius;
    }
    /**
     * Calculate Sphere surface area
     * @return Sphere surface area value
     */
    public double getSurfaceArea(){
        return 4*Math.PI*Math.pow(radius, 2);
    }
    /**
     * Calculate Sphere volume
     * @return Sphere's Volume value
     */
    public double getVolume(){
        return 4/3*Math.PI*Math.pow(radius, 3);
    }
    public double getRadius() {
        return radius;
    }

}
