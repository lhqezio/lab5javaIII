package geometry;
import static org.junit.Assert.assertEquals;
import org.junit.Test;


public class SphereTest {
    double marginError = 0.1;
    @Test
    public void constorTest(){
        Sphere s = new Sphere(30);
        assertEquals(30, s.getRadius(),marginError);
    }
    @Test
    public void VolumeTest(){
        Sphere s = new Sphere(3);
        assertEquals(33.51, s.getVolume(),marginError);
    }
    @Test
    public void SAreaTest(){
        Sphere s = new Sphere(3);
        assertEquals(113.09734, s.getSurfaceArea(),marginError);
    }
}
