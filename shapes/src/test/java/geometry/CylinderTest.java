package geometry;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CylinderTest {
    final double DELTA = 0.01;
    @Test
    /**
     * Test Constructor Parameters
     */
    public void paramTest(){
        Cylinder c = new Cylinder(3, 5);
        assertEquals(c.getHeight(), 5,DELTA);
        assertEquals(c.getRadius(), 3,DELTA);
    }
    /**
     * Test Volume method
     */
    @Test
    public void volumeTest(){
        Cylinder c = new Cylinder(3, 5);
        assertEquals(141.37, c.getVolume(),DELTA);   
    }
    /**
     * Test Surface Area
     */
    @Test
    public void surfaceAreaTest(){
        Cylinder c = new Cylinder(3, 5);
        assertEquals(c.getSurfaceArea(),(48*Math.PI),DELTA );
    }
}
